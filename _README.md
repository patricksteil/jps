Hi Patrick,

This is an updated version of the Magento cluster.
Original version:  https://1drv.ms/u/s!AjXNSgT_Jsyml5hqRttAlOOFy0ok4Q

Deploy from your Dashboard using the Import option.

This JPS file will only deploy the cluster topology 
(Nginx load balancer, Nginx App Servers, MySQL database Master + Slave, Storage Node) 
without the Redis nodes. 

Also, The NFS mounts are updated, so the app servers only share the pub/ and var/ folders.

By default, it also installs a fresh Magento 2 instance, so you will have to manually 
import the data from your dev websites. If you wish, you could further automate that by
extending the JPS Manifest. Documentation for this is available at http://docs.cloudscripting.com/ .


# changelog

wd-magento-cluster-000.yaml - original base template from Jelastic - tested, works
wd-magento-cluster-001.yaml - added ENV vars to AppServers 
    mroot : $WEBROOT/ROOT
    droot : $WEBROOT/deploy
    TZ : America/Chicago
wd-magento-cluster-003.yaml - added more ENV vars to AppServers - works
wd-magento-cluster-004.yaml
    - naming appServer MagentoServer and Load balancer LoadBalancer
wd-magento-cluster-005.yaml
    - reduce number of MagentoServer nodes to 1
    - try to set env vars: echo flag=jcli >/etc/environment - didn't work, backed it out


# troubleshooting:

- JPS manifest is corrupted! JSON.parse - don't forget to push the .jps file to gitlab!
